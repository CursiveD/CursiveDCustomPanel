I have created a custom panel for TVPaint 11 for Windows, MacOSX and Linux. This custom panel is to easy certain task during the animation process. This project was created in partnership with Matt Stoehr (formandspace.com)

I am releasing it for free under a Creative Commons Attribution-Non Commercial License. If you need a commercial license please contact me here.